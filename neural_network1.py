import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_derivative(x):
    return x * (1 - x)


def generate_weights(shape):

    weight_arrays = []
    for i in range(0, len(shape) - 1):
        #weight_array = 2*np.random.rand(shape[i+1], shape[i]) - 1 #uniform
        weight_array = np.random.rand(shape[i+1], shape[i])*np.sqrt(2/shape[i])
        weight_arrays.append(weight_array.astype(np.float32))

    return weight_arrays


def forward_propagation(input, weights):

    current_input = input
    outputs = []
    for weight in weights:
        current_net = np.dot(weight, current_input)
        current_output = sigmoid(current_net)
        outputs.append(current_output)
        current_input = current_output

    return current_output.T


def train_network(input, output, training_rate, weights, validation_inputs, validation_outputs):

    accuracy = []
    for i in range(30000):
        weight_arrays = back_propagation(input, output, training_rate, weights)
        if validation_inputs is not None:
            trained_output = forward_propagation(validation_inputs, weights)
            for j in range(len(trained_output)):
                idx = np.argmax(trained_output[j,:])
                trained_output[j,:] = 0
                trained_output[j,idx] = 1
            acc = (trained_output == validation_outputs.T).all(axis=1).astype(int).sum()/len(trained_output) * 100
            print(f'step {i}: acc = {acc} %.')
            accuracy = np.append(accuracy, acc)
            plt.scatter(i, acc, c='b')

    plt.ylabel("accuracy [%]")
    plt.xlabel("step")
    plt.show()
    return weight_arrays


def back_propagation(input, output, training_rate, weights):

    current_input = input
    outputs = []
    for weight in weights:
        current_net = np.dot(weight, current_input)
        current_output = sigmoid(current_net)
        outputs.append(current_output)
        current_input = current_output

    deltas = []
    error = output - outputs[len(outputs)-1]
    delta = error * sigmoid_derivative(outputs[len(outputs)-1])
    deltas.append(delta)

    current_delta = delta
    back_idx = len(outputs) - 2

    for weight in weights[::-1][:-1]:
        next_error = np.dot(weight.T, current_delta)
        next_delta = next_error * sigmoid_derivative(outputs[back_idx])
        deltas.append(next_delta)
        current_delta = next_delta
        back_idx -= 1

    cur_weight_idx = len(weights) - 1

    for delta in deltas:
        input_used = None
        if cur_weight_idx - 1 < 0:
            input_used = input
        else:
            input_used = outputs[cur_weight_idx - 1]

        weights[cur_weight_idx] += 10*training_rate*np.dot(delta, input_used.T) / input.shape[1]
        cur_weight_idx -= 1

    return weights
