import os
import gzip
import _pickle as cPickle
import wget
import numpy as np
from neural_network1 import generate_weights, train_network, forward_propagation


def vectorized_result(y):
    e = np.zeros((10,))
    e[y] = 1.0
    return e

def load_mnist():
    if not os.path.exists(os.path.join(os.curdir, 'data')):
        os.mkdir(os.path.join(os.curdir, 'data'))
        wget.download('http://deeplearning.net/data/mnist/mnist.pkl.gz', out='data')

    data_file = gzip.open(os.path.join(os.curdir, 'data', 'mnist.pkl.gz'), 'rb')
    training_data, validation_data, test_data = cPickle.load(data_file, encoding='latin1')
    data_file.close()

    training_inputs = np.array([np.reshape(x, (784,)) for x in training_data[0]]).T
    training_results = np.array([vectorized_result(y) for y in training_data[1]]).T

    validation_inputs = np.array([np.reshape(x, (784,)) for x in validation_data[0]]).T
    validation_outputs = np.array([vectorized_result(y) for y in validation_data[1]]).T

    test_inputs = np.array([np.reshape(x, (784,)) for x in test_data[0]]).T
    test_outputs = np.array([vectorized_result(y) for y in test_data[1]]).T

    return training_inputs, training_results, test_inputs, test_outputs, validation_inputs, validation_outputs

def load_mnist_preprocessed():
    path = os.path.join(os.curdir, 'data', 'mnist_preprocessed.npz')
    data = np.load(path)
    return data['training_inputs'], data['training_outputs'], data['test_inputs'], data['test_outputs']

def save_mnist_preprocessed(training_inputs, training_outputs, test_inputs, test_outputs, validation_inputs, validation_outputs):
    path = os.path.join(os.curdir, 'data', 'mnist_preprocessed')
    np.savez_compressed(path,
            training_inputs=training_inputs,
            training_outputs=training_outputs,
            test_inputs=test_inputs,
            test_outputs=test_outputs,
            validation_inputs = validation_inputs,
            validation_outputs = validation_outputs)

if __name__ == '__main__':
    try:
        training_inputs, training_outputs, test_inputs, test_outputs, validation_inputs, validation_outputs = load_mnist_preprocessed()
    except:
        training_inputs, training_outputs, test_inputs, test_outputs, validation_inputs, validation_outputs = load_mnist()
        save_mnist_preprocessed(training_inputs, training_outputs, test_inputs, test_outputs, validation_inputs, validation_outputs)

    shape = [784, 397, 397, 10]
    training_rate = 0.1

    weights = generate_weights(shape)
    weights = train_network(training_inputs, training_outputs, training_rate, weights, validation_inputs, validation_outputs)
    test_output_pred = forward_propagation(test_inputs, weights)
    for j in range(len(test_output_pred)):
        idx = np.argmax(test_output_pred[j,:])
        test_output_pred[j,:] = 0
        test_output_pred[j,idx] = 1
    acc = (test_output_pred == test_outputs.T).all(axis=1).astype(int).sum()/len(test_output_pred) * 100
    print(f'acc = {acc} %.')
