from neural_network1 import generate_weights, train_network, forward_propagation
import numpy as np
import pandas as pd
from random import sample

from itertools import product 
x = np.arange(start=-1.0, stop=1, step=0.01).astype(float).round(2)
pairs = np.array(list(product(x, x)))
quarter = np.zeros((len(pairs), 4))
for i in range (0, len(pairs)):

    if pairs[i][0] >= 0 and pairs[i][1] >= 0:
        quarter[i] = [1,0,0,0]
        
    elif pairs[i][0] < 0 and pairs[i][1] >= 0:
        quarter[i] = [0,1,0,0]
        
    elif pairs[i][0] < 0 and pairs[i][1] < 0:
        quarter[i] = [0,0,1,0]
        
    elif pairs[i][0] >= 0 and pairs[i][1] < 0:
        quarter[i] = [0,0,0,1]       

input_data = np.array(pairs).T
output_data = np.array(quarter).T

shape = [2, 3, 3, 4]

weights = generate_weights(shape)
training_rate = 0.1


#Accuracy of training data
weights = train_network(input_data, output_data, training_rate, weights, input_data, output_data)

idx = np.random.choice(np.arange(len(pairs)), 8000, replace=False)
train_input = np.array(pairs[idx]).T
train_output = np.array(quarter[idx]).T

train_output_pred = forward_propagation(train_input, weights)
for j in range(len(train_output_pred)):
    idx = np.argmax(train_output_pred[j,:])
    train_output_pred[j,:] = 0
    train_output_pred[j,idx] = 1                          
acc = (train_output_pred == train_output.T).all(axis=1).astype(int).sum()/len(train_output_pred) * 100
print(f'acc = {acc} %.')
print(train_output_pred)
print(train_output.T)


#Accuracy of test data
indices = sample(range(len(pairs)),8000)
test_input = np.array(pairs[indices, :]).T
train_input = np.array(np.delete(pairs,indices,0)).T

test_output = np.array(quarter[indices, :]).T
train_output = np.array(np.delete(quarter,indices, 0)).T
print(len(train_input.T))

weights = generate_weights(shape)
weights = train_network(train_input, train_output, training_rate, weights)
test_output_pred = forward_propagation(test_input, shape, weights)
for j in range(len(test_output_pred)):
    idx = np.argmax(test_output_pred[j,:])
    test_output_pred[j,:] = 0
    test_output_pred[j,idx] = 1                          
acc = (test_output_pred == test_output.T).all(axis=1).astype(int).sum()/len(test_output_pred) * 100
print(f'acc = {acc} %.')





